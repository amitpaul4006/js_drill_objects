const keys = (testObject) => {
    let listOfKeys = []
    for(let key in testObject) {
        listOfKeys.push(key)
    }
    return listOfKeys
}

module.exports = keys;

