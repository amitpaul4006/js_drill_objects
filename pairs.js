const pairs = (objectData) => {
    let listObject = [];
    for(let key in objectData) {
        listObject.push([key, objectData[key]]);
    }
    return listObject;
}

module.exports = pairs;