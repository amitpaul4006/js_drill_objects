const testObject = require('../data');
const functionKey = require('../values');

const resultOfValues = functionKey(testObject);
const expectedValues = [ 'Bruce Wayne', 36, 'Gotham' ]

let count = 0

for(let i=0; i<resultOfValues.length; i++) {
    if(resultOfValues[i] === expectedValues[i]){
        count++
    }
}

if(count === expectedValues.length) console.log(resultOfValues)