const testObject = require('../data');
const functionKey = require('../keys');

const resultOfKey = functionKey(testObject);
const expectedKeys = [ 'name', 'age', 'location' ]
let count = 0

for(let i=0; i<resultOfKey.length; i++) {
    if(resultOfKey[i] === expectedKeys[i]){
        count++
    }
}

if(count === expectedKeys.length) console.log(resultOfKey)