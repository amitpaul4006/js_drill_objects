const objectData = require('../data');
const mapObject = require('../mapObject')

const callback = (value) => {
    let new_values = value + ' ' + 'this is added' + ':' + '1';
    return new_values;
}

let expectedValues = { name: 'Bruce Wayne this is added:1',
                        age: '36 this is added:1',
                        location: 'Gotham this is added:1' }  

const resultOfMap = mapObject(objectData,callback)
let finalResult = {}


for(let key in resultOfMap){
    if(resultOfMap[key] === expectedValues[key]){
        finalResult[key] = resultOfMap[key]
    }
}

console.log(finalResult)