const invert = (objectData) => {
    let invert_object = {}
    
    for(let key in objectData){
        invert_object[objectData[key]] = key
    }
    return invert_object
}


module.exports = invert;