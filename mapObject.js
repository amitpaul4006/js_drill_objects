const mapObject = (objectData,callback) => {
    let resultObject = {};
    for(let key in objectData) {
        resultObject[key] = callback(objectData[key])
    }
    return resultObject
}
module.exports = mapObject;