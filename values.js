const values = (testObject) => {
    let listOfValues = [];
    for(let key in testObject) {
        listOfValues.push(testObject[key]);
    }
    return listOfValues;
}

module.exports = values;

